#!/bin/bash

dir=memsan_results

get_num_objects() {
    local dir="$1"

    echo $dir | sed 's/.*rt21-\([0-9]\+\)-+/\1/'
}

get_sample_intervals() {
    local dir="$1"
    local list_files

    for file in $(get_file_list $dir); do
        list_files+="$(echo $file | cut -d '_' -f 3) "
    done

    echo $list_files | tr ' ' '\n' | sort --numeric-sort --unique
}

get_dir_list() {
    local root="$1"

    find $root -mindepth 1 -maxdepth 1 -type d
}

get_file_list() {
    local dir="$1"

    find $dir -type f
}

get_file_list_filtered() {
    local dir="$1"
    local si="$2"

    get_file_list $dir | grep dmesg_$si\_
}

main() {
    declare -a list_dirs
    declare -a list_sample_interval

    local list_filtered

    local elem
    local avg
    local cnt
    local n
    local d
    local s

    for elem in $(get_dir_list $dir); do
        list_dirs+=($elem)
    done

    for elem in $(get_sample_intervals "${list_dirs[0]}"); do
        list_sample_interval+=($elem)
    done

    printf "num_objects,sample_interval,num_events_detected\n"
    for d in "${list_dirs[@]}"; do
        for s in "${list_sample_interval[@]}"; do
            n=0
            avg=0
            list_filtered=$(get_file_list_filtered $d $s)
            for elem in $list_filtered; do
                cnt=$(grep -c "BUG: KFENCE" $elem)
                avg=$((avg+cnt))
                n=$((n+1))
            done
            avg=$(echo "$avg / $n" | bc -l)
            printf "%d,%d,%.2f\n" $(get_num_objects $d) $s $avg
        done
    done
}

main "${@}"

